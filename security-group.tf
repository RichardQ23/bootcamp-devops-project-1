resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Permitir acesso remoto via porta 22 (ssh)"
  # vpc_id      = aws_vpc.main.id

  ingress {
    description      = "ssh FROM VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    # ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }

  tags = {
    Name = "allow_SSH"
  }
}

resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Permitir acesso remoto via porta 80 (http)"
  # vpc_id      = aws_vpc.main.id

  ingress {
    description      = "http FROM VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    # ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }

  tags = {
    Name = "allow_http"
  }
}


resource "aws_security_group" "allow_egress" {
  name        = "allow_egress"
  description = "Permitir acesso remoto via porta 80 (egress)"
  # vpc_id      = aws_vpc.main.id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_egress"
  }
}