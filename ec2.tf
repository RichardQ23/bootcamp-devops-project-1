resource "aws_instance" "amb-prod" {
  ami           = "ami-0c7217cdde317cfec"
  instance_type = "t2.micro"
  key_name = "Terraform"
  security_groups = [ "allow_ssh", "allow_http", "allow_egress" ]
  user_data = file("script.sh")


  tags = {
    Name = "amb-prod"
  }
}

# resource "aws_instance" "Windows_Server" {
#   ami           = "ami-06938c7701be658b4"
#   instance_type = "t2.micro"
#   security_groups = [ "allow_ssh", "allow_http", "allow_egress" ]
#   key_name = "windows"
#   user_data = file("script.sh")

#   # ... outras configurações ...
# }
